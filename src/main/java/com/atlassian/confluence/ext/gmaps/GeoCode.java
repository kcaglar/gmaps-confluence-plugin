package com.atlassian.confluence.ext.gmaps;

import java.io.Serializable;

/**
 * A simple bean to represent a GeoCode
 */
public class GeoCode implements Serializable
{
    private double latitude;
    private double longitude;
    private String info;

    public GeoCode(double latitude, double longitude)
    {
        this( latitude, longitude, "");
    }
    
    public GeoCode(double latitude, double longitude, String info)
        {
        this.latitude = latitude;
        this.longitude = longitude;
        this.info = info;
    }

    
    public double getLatitude()
    {
        return latitude;
    }
    
    public double getLongitude()
    {
        return longitude;
    }
    
    public String getInfo()
    {
        return this.info;
    }
    
    public String toString()
    {
        return this.latitude + "," + this.longitude;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof GeoCode)
        {
            GeoCode that = (GeoCode) obj;
            return this.latitude == that.latitude && this.longitude == that.longitude;
        }
        
        return false;
    }
}
