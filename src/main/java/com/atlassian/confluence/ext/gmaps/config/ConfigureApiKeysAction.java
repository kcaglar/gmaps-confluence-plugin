package com.atlassian.confluence.ext.gmaps.config;


import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.ext.gmaps.ApiKeyManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import java.util.List;

@WebSudoRequired
public class ConfigureApiKeysAction extends ConfluenceActionSupport
{
    private ApiKeyManager apiKeyManager;

    private String apiKey;

    @SuppressWarnings("unused")
    public void setApiKeyManager(ApiKeyManager apiKeyManager)
    {
        this.apiKeyManager = apiKeyManager;
    }

    @SuppressWarnings("unused")
    public String getApiKey()
    {
        return apiKey;
    }

    public void setApiKey(String apiKey)
    {
        this.apiKey = apiKey;
    }

    @Override
    protected List<String> getPermissionTypes()
    {
        List<String> requiredPermissions = super.getPermissionTypes();
        requiredPermissions.add(SpacePermission.CONFLUENCE_ADMINISTRATOR_PERMISSION);

        return requiredPermissions;
    }

    @Override
    public String doDefault() throws Exception
    {
        setApiKey(apiKeyManager.getKey());
        return super.doDefault();
    }

    @Override
    public String execute() throws Exception
    {
        apiKeyManager.setKey(getApiKey());
        addActionMessage(getText("gmap.admin.apikey.message.apikeysaved"));
        return SUCCESS;
    }
}
