package com.atlassian.confluence.ext.gmaps;

import org.apache.commons.lang.StringUtils;

public class Address
{
    String address;
    String info;
    
    public Address( String address, String info )
    {
        this.address = address;
        this.info = info;
    }

    public String getAddress()
    {
        return address;
    }

    public String getInfo()
    {
        return info;
    }

}
