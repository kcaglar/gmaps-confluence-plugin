package com.atlassian.confluence.ext.gmaps;

import org.apache.commons.lang.exception.NestableException;

/**
 * An exception covering all manner of GeoCoding errors.
 */
public class GeoCodeException extends NestableException
{
    public GeoCodeException()
    {
    }

    public GeoCodeException(String string)
    {
        super(string);
    }

    public GeoCodeException(Throwable throwable)
    {
        super(throwable);
    }

    public GeoCodeException(String string, Throwable throwable)
    {
        super(string, throwable);
    }
}
