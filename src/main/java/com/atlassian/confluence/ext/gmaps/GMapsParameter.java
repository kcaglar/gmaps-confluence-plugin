package com.atlassian.confluence.ext.gmaps;

public enum GMapsParameter
{
    NAME(null), 
    WIDTH( new Integer(700) ), 
    HEIGHT( new Integer(500) ), 
    STYLE( ""), 
    ZOOM( new Integer(13) ),
    MAPTYPE("map,satellite,hybrid"),
    DISPLAYMAP("map"),
    CONTROLS("auto"),
    MAPSCALE(Boolean.TRUE),
    MAPTYPECONTROL(Boolean.TRUE),
    OVERVIEW(Boolean.FALSE),
    DRAG(Boolean.TRUE),
    WHEELZOOM(Boolean.FALSE),
    DOUBLECLICKZOOM(Boolean.TRUE),
    MARKER(Boolean.TRUE);
    
    private Object defaultValue = null;
    
    
    private GMapsParameter(Object defaultValue) 
    {
        this.defaultValue = defaultValue;
    }

    
    public Object getDefaultValue()
    {
        return defaultValue;
    }
    
    public String toString()
    {
        return name().toLowerCase();
    }
}
