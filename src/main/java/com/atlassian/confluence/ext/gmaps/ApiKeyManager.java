package com.atlassian.confluence.ext.gmaps;


public interface ApiKeyManager
{
    String getKey();

    void setKey(String key);
}
