package com.atlassian.confluence.ext.gmaps;

public class Directions
{
    private String fromAddress;
    private String toAddress;
    
    public Directions(String from, String to )
    {
        this.fromAddress = from;
        this.toAddress = to;
    }

    public String getFromAddress()
    {
        return fromAddress;
    }

    public String getToAddress()
    {
        return toAddress;
    }
}
