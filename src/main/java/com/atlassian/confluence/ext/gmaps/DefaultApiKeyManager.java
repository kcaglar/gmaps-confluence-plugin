package com.atlassian.confluence.ext.gmaps;


import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.apache.commons.lang.StringUtils;

public class DefaultApiKeyManager implements ApiKeyManager
{
    public static final String BANDANA_KEY_GMAP_API_V3_KEY = "googleMapsV3ApiKey";
    private final BandanaManager bandanaManager;

    public DefaultApiKeyManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }

    @Override
    public String getKey()
    {
        return (String) bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, BANDANA_KEY_GMAP_API_V3_KEY);
    }

    @Override
    public void setKey(String key)
    {
        if (StringUtils.isBlank(key))
            bandanaManager.removeValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, BANDANA_KEY_GMAP_API_V3_KEY);
        else
            bandanaManager.setValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, BANDANA_KEY_GMAP_API_V3_KEY, key);
    }
}
