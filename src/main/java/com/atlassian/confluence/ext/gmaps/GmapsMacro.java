package com.atlassian.confluence.ext.gmaps;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GmapsMacro extends BaseMacro implements Macro
{
    private static final Logger LOG = LoggerFactory.getLogger(GmapsMacro.class);

    private static final String DEFAULT_TEMPLATE = "/templates/gmaps/gmaps-macro.vm";
    private static final String PARAM_ERROR_TEMPLATE = "/templates/gmaps/gmaps-errors.vm";
    
    private static final String LOCATIONS_PARAM = "locations";
    private static final String ADDRESSES_PARAM = "addresses";
    private static final String DIRECTIONS_PARAM = "directions";

    private static final String DIRECTIONS_PREFIX = "directions:";
    private static final String PLACE_PREFIX = "place:";

    private static final String CENTER_ADDRESS_PARAM = "centerAddress";

    private static final String CENTER_LOCATION_PARAM = "centerLocation";

    private static final int BIG_HEIGHT = 400;

    private static final int MEDIUM_HEIGHT = 200;

    private final VelocityHelperService velocityHelperService;

    private final LocaleManager localeManager;

    private final I18NBeanFactory i18NBeanFactory;

    private final ApiKeyManager apiKeyManager;

    public GmapsMacro(VelocityHelperService velocityHelperService, LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, ApiKeyManager apiKeyManager)
    {
        this.velocityHelperService = velocityHelperService;
        this.localeManager = localeManager;
        this.i18NBeanFactory = i18NBeanFactory;
        this.apiKeyManager = apiKeyManager;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    @Override
    @SuppressWarnings("unchecked")
    public String execute(Map macroParameters, String macroBody, RenderContext renderContext)
    {
        return execute(macroParameters, macroBody, new DefaultConversionContext(renderContext));
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.PLAIN_TEXT;
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext)
    {
        try
        {
            String templateName = DEFAULT_TEMPLATE;
            Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
            velocityContext.put("remoteUser", AuthenticatedUserThreadLocal.getUser());
                
            // Parse all the macro params
            List<String> invalidParams = parseMacroParams(parameters, velocityContext);
            velocityContext.put("invalidParams", invalidParams);

            // Fill in the body and all the non-parameter values
            parseMacroBody(body, velocityContext);
            
            // Fix up/validate the parameter values
            List<String> validationErrors = validateParams(velocityContext);
            velocityContext.put("validationErrors", validationErrors);

            velocityContext.put("lang", localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()).getLanguage());
            velocityContext.put("apiKey", StringUtils.defaultString(StringUtils.trim(apiKeyManager.getKey())));
            
            // Check for any error states
            if( invalidParams.size() != 0 || validationErrors.size() != 0 )
            {
                templateName = PARAM_ERROR_TEMPLATE;
            }

            // Render the Velocity Template
            return velocityHelperService.getRenderedTemplate(templateName, velocityContext);
        }
        catch (Exception unableToRenderMap)
        {
            LOG.error(String.format("Unable to render Google Maps based on specified location: %s", body), unableToRenderMap);
            return RenderUtils.blockError(getText("gmaps.errors.unabletolocateaddr", Arrays.asList(GeneralUtil.htmlEncode(body))), "");
        }
    }

    private String getText(String key)
    {
        return getText(key, Collections.emptyList());
    }

    private String getText(String key, List<?> substitutions)
    {
        return getI18NBean().getText(key, substitutions);
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    /*
     * Pull, parse, and validate the macro parameters
     */
    protected List<String> parseMacroParams(Map<String, String> parameters, Map<String, Object> velocityContext)
    {
        List<String> invalidArguments = new ArrayList<String>();

        // populate context with all default values
        for (GMapsParameter param : GMapsParameter.values())
        {
            velocityContext.put(param.toString(), param.getDefaultValue());
        }

        // for each parameter
        for (Map.Entry<String, String> param : parameters.entrySet())
        {
            try
            {
                // validate name
                GMapsParameter enumEntry = GMapsParameter.valueOf(param.getKey().toUpperCase());
                
                // parse/validate value
                Object value = param.getValue();
                if (enumEntry.getDefaultValue() instanceof Integer)
                {
                    value = Integer.parseInt(param.getValue());
                }
                else if( enumEntry.getDefaultValue() instanceof Boolean )
                {
                    value = Boolean.parseBoolean(param.getValue());
                }
                
                // store value in context
                velocityContext.put(enumEntry.toString(), value);
            }
            catch( IllegalArgumentException e )
            {
                if( !param.getKey().equals(com.atlassian.renderer.v2.macro.Macro.RAW_PARAMS_KEY))
                    invalidArguments.add( param.getKey() );
            }
        }
        
        return invalidArguments;
    }
    
    protected List<String> validateParams(Map<String, Object> velocityContext)
    {
        List<String> errors = new ArrayList<String>();
        
        // Do some fixups
        String controls = (String) velocityContext.get(GMapsParameter.CONTROLS.toString());
        if( controls.equals("auto"))
        {
            int height = (Integer) velocityContext.get(GMapsParameter.HEIGHT.toString());
            if( height > BIG_HEIGHT)
                controls = "large";
            else if( height > MEDIUM_HEIGHT )
                controls = "medium";
            else
                controls = "small";
            
            velocityContext.put(GMapsParameter.CONTROLS.toString(), controls);
        }
        
        // GMaps likes its maps names capitalized
        String displayMap = (String) velocityContext.get(GMapsParameter.DISPLAYMAP.toString());
        displayMap = WordUtils.capitalizeFully(displayMap);
        velocityContext.put(GMapsParameter.DISPLAYMAP.toString(), displayMap);
        
        String[] mapTypes = ((String) velocityContext.get(GMapsParameter.MAPTYPE.toString())).split(",");
        for (int i = 0; i < mapTypes.length; i++) 
        {
			mapTypes[i] = WordUtils.capitalizeFully(mapTypes[i]);
		}
        velocityContext.put(GMapsParameter.MAPTYPE.toString(), splice(mapTypes));
        
        // Check for a center location
        List<Directions> directions = (List<Directions>) velocityContext.get(DIRECTIONS_PARAM);
        if( (velocityContext.get(CENTER_ADDRESS_PARAM) == null) 
                && velocityContext.get(CENTER_LOCATION_PARAM) == null 
                && (directions == null || directions.size() == 0) ) 
        {
        	errors.add(getText("gmaps.errors.no.content") );
        }
        
        return errors;
    }
    
    
    private String splice(String[] mapTypes) {
    	String prefix = "";
    	StringBuffer result = new StringBuffer();
    	for (int i = 0; i < mapTypes.length; i++) {
			result.append(prefix).append(mapTypes[i]);
			prefix = ",";
		}
    	
		return result.toString();
	}

	/*
     * Populate the velocity context with GeoCode objects for known/specified addresses,
     * Address objects for unknown addresses, and Direction objects for, well, you know.
     */
    protected void parseMacroBody(String body, Map<String, Object> velocityContext)
    {
        List<GeoCode> locations = new ArrayList<GeoCode>();
        List<Address> addresses = new ArrayList<Address>();
        List<Directions> directions = new ArrayList<Directions>();
        
        Object center = null;
        
        // Parse the content
        if(StringUtils.isNotBlank(body))
        {
            BufferedReader reader = new BufferedReader( new StringReader(body));
            try
            {
                while( reader.ready())
                {
                    String bodyEntry = reader.readLine();
                    if( bodyEntry == null )
                        break;
                    
                    if( !StringUtils.isBlank(bodyEntry))
                    {
                        Object result = parseEntry(bodyEntry);
                        if( result != null )
                        {
                            // Save off the first one (but not a directions object)
                            if( center == null && !(result instanceof Directions ))
                            {
                                center = result;
                            }
                            else
                            {
                                if (result instanceof GeoCode)
                                {
                                    locations.add((GeoCode) result);
                                }
                                else if( result instanceof Directions )
                                {
                                    directions.add((Directions) result);
                                }
                                else if (result instanceof Address)
                                {
                                    addresses.add((Address) result);
                                }
                            }
                        }
                    }
                }
            }
            catch (IOException e)
            {
                // This should never happen since we're reading from a string
            }
        }
        
        velocityContext.put(LOCATIONS_PARAM, locations);
        velocityContext.put(ADDRESSES_PARAM, addresses);
        velocityContext.put(DIRECTIONS_PARAM, directions);
        
        if(center instanceof GeoCode)
            velocityContext.put(CENTER_LOCATION_PARAM, center);
        else
            velocityContext.put(CENTER_ADDRESS_PARAM, center);
    }
    
    protected Object parseEntry(String entry)
    {
        Object result = null;
        
        // determine type
        if( entry.startsWith(DIRECTIONS_PREFIX))
        {
            result = parseDirection(entry.substring(DIRECTIONS_PREFIX.length()));
        }
        else 
        {
            if(entry.startsWith(PLACE_PREFIX))
                entry = entry.substring(PLACE_PREFIX.length());
            result = parseAddress(entry);
        }
        
        return result;
    }

    protected Object parseAddress(String entry)
    {
        if( StringUtils.isBlank(entry))
            return null;

        Object result = null;
        
        // split into fields
        Map<String, String> fields = parseFields(entry);
        
        // find content
        String addressValue = fields.get("address");
        // if the format matches latlon, parse latlon
        if( addressValue.matches("-?[\\d|\\.]+,-?[\\d|\\.]+"))
        {
            String[] values = addressValue.split(",");
            result = new GeoCode( Double.parseDouble(values[0]), Double.parseDouble(values[1]), fields.get("info"));
        }
        else
        {
            // otherwise
                // clean up address
                // look up latlon in cache
            result = new Address( addressValue, StringEscapeUtils.escapeHtml(fields.get("info")));
        }
        
        return result;
    }
    
    protected Object parseDirection(String entry)
    {
        if( StringUtils.isBlank(entry))
            return null;

        Object result = null;
        
        // split into fields
        Map<String, String> fields = parseFields(entry);
        result = new Directions( fields.get("from"), fields.get("to" ));
        
        return result;
    }
    
    protected Map<String, String> parseFields(String entry)
    {
        Map<String, String> result = new HashMap<String, String>();
        
        String[] fields;
        if( entry.contains("|"))
            fields = entry.split("\\|");
        else
            fields = new String[]{ entry };

        for (String field : fields)
        {
            // Default values
            String key = "address";
            String value = field;
            
            // see if there's a simple label= prefix
            if(field.matches("^\\p{Alpha}+=.*"))
            {
                int equalsIndex = field.indexOf('=');
                key = field.substring(0, equalsIndex);
                value = field.substring(equalsIndex + 1);
            }

            result.put( key, value );
        }
        
        return result;
    }
    
    protected String capitalizeName(String name) {
        char[] chars = name.toLowerCase().toCharArray();
        String first = (chars[0]+"").toUpperCase();
        chars[0] = first.toCharArray()[0];
        return String.valueOf(chars);
    }
}