package com.atlassian.confluence.ext.gmaps;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.definition.PlainTextMacroBody;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import org.apache.commons.lang.StringUtils;

public class GMapMacroMigrator implements MacroMigration
{
    @Override
    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        macroDefinition.setBody(new PlainTextMacroBody(StringUtils.defaultString(macroDefinition.getBodyText())));
        return macroDefinition;
    }
}
