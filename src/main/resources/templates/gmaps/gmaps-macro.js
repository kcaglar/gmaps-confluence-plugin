;jQuery(function($) {
    var GoogleMaps = {
        init : function($mapContainer) {
            var params = this.getFieldSetAsParams($mapContainer),
                centerLocation =  this.getMarkerObject($("input[name='centeraddress']", $mapContainer)) || this.getMarkerObject($("input[name='centerlocation']", $mapContainer)),
                draggable = params["drag"] === "true",
                showMarker = params["marker"] === "true",
                mapOptions = {
                    "zoom": parseInt(params["zoom"]),
                    "mapTypeControl" : params["maptypecontrol"] === "true",
                    "mapTypeControlOptions" : {
                        "mapTypeIds" : GoogleMaps.getMapTypes(params["maptype"].split(","))
                    },
                    "mapTypeId" : GoogleMaps.getMapTypes(params["displaymap"]),
                    "panControl" : params["controls"] !== "none",
                    "zoomControl" : params["controls"] !== "none",
                    "scaleControl" : params["mapscale"] === "true",
                    "overviewMapControl" : params["overview"] === "true",
                    "draggable" : draggable,
                    "disableDoubleClickZoom" : !draggable || params["doubleclickzoom"] !== "true",
                    "scrollwheel" : draggable && params["wheelzoom"] === "true"
                };

            if (centerLocation) {
                this.getLonLng(centerLocation.location, function(geocoderResults) {
                    if ($.isArray(geocoderResults) && geocoderResults.length) {
                        mapOptions.center = geocoderResults[0].geometry.location;
                        GoogleMaps.setMapToContainer(
                            $mapContainer,
                            new google.maps.Map(
                                GoogleMaps.getMapCanvas($mapContainer)[0],
                                mapOptions
                            )
                        );

                        if (showMarker) {
                            GoogleMaps.createMarkerOnMap($mapContainer, mapOptions.center, geocoderResults[0].formatted_address);
                            GoogleMaps.showAdditionalLocationsOnMap($mapContainer);
                        }

                        GoogleMaps.showDirectionsOnMap($mapContainer);
                    }
                });
            } else if ($(".directions", $mapContainer).length) {
                GoogleMaps.setMapToContainer(
                    $mapContainer,
                    new google.maps.Map(
                        GoogleMaps.getMapCanvas()[0],
                        mapOptions
                    )
                );
                GoogleMaps.showDirectionsOnMap($mapContainer);
            } else {
                AJS.messages.error(
                    GoogleMaps.getMapCanvas().empty(),
                    {
                        body: "<p>" + $("<div/>", { "text" : AJS.I18n.getText("gmaps.errors.invalidmaplocation") }).html() + "</p>",
                        closeable : false
                    }
                );
            }
        },

        showDirectionsOnMap : function($mapContainer) {
            var directions = $.map(
                    $(".directions", $mapContainer),
                    function(direction) {
                        var $direction = $(direction);
                        return {
                            "from" : $direction.val(),
                            "to" : $direction.attr("title")
                        }
                    }
                ),
                directionsDisplay,
                directionsService;

            if (directions && directions.length) {
                directionsDisplay = new google.maps.DirectionsRenderer();
                directionsDisplay.setMap(this.getMapFromContainer($mapContainer));

                directionsService = new google.maps.DirectionsService();
                $.each(directions, function(directionIdx, direction) {
                    directionsService.route({
                        "origin" : direction.from,
                        "destination" : direction.to,
                        "travelMode" : google.maps.DirectionsTravelMode.DRIVING
                    }, function(directionsResult, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            directionsDisplay.setDirections(directionsResult);
                        }
                    });
                });
            }
        },

        showAdditionalLocationsOnMap : function($mapContainer) {
            var locationObjects = $.map($(".address, .location", $mapContainer), function(locationField) {
                return GoogleMaps.getMarkerObject($(locationField));
            });

            if (locationObjects && locationObjects.length) {
                $.each(locationObjects, function(locationObjectIdx, locationObject) {
                    if (locationObject) {
                        GoogleMaps.getLonLng(
                            locationObject.location,
                            function(geocoderResults) {
                                if (geocoderResults && geocoderResults.length) {
                                    $mapContainer.data(
                                        "additionalMarkers",
                                        $.map(geocoderResults, function(geocoderResult) {
                                            return GoogleMaps.createMarkerOnMap(
                                                $mapContainer,
                                                geocoderResult.geometry.location,
                                                geocoderResult.formatted_address
                                            );
                                        })
                                    );
                                }
                            }
                        );
                    }
                })
            }
        },

        createMarkerOnMap : function($mapContainer, position, title) {
            var theMap = GoogleMaps.getMapFromContainer($mapContainer),
                aMarker = new google.maps.Marker({
                    map: theMap,
                    position: position,
                    title : title
                }),
                getCurrentPopup = function() {
                    return $mapContainer.data("markerPopup");
                },
                setCurrentPopup = function(markerPopup) {
                    if (markerPopup)
                        $mapContainer.data("markerPopup", markerPopup);
                    else
                        $mapContainer.removeData("markerPopup");
                };

            google.maps.event.addListener(aMarker, "click", function() {
                var markerPopup = getCurrentPopup();
                if (markerPopup) {
                    markerPopup.close();
                    setCurrentPopup();
                }

                markerPopup = new google.maps.InfoWindow({
                    "content" : $("<div/>").append($("<strong/>", { "text" : AJS.format(AJS.I18n.getText("gmaps.marker.info"), title)} )).html(),
                    "maxWidth" : 300
                });
                markerPopup.open(theMap, aMarker);
                setCurrentPopup(markerPopup);
            });
        },

        getMapFromContainer : function($mapContainer) {
            return $mapContainer.data("map");
        },

        getMapCanvas : function($mapContainer) {
            return $(".gmap-canvas", $mapContainer);
        },

        setMapToContainer : function($mapContainer, theMap) {
            $mapContainer.data("map", theMap);
        },

        getLonLng : function(address, callback) {
            if (typeof address === "string") {
                new google.maps.Geocoder().geocode({
                    "address" : address
                }, function(geocoderResults) {
                    if ($.isFunction(callback)) {
                        callback(geocoderResults);
                    }
                });
            } else if ($.isArray(address)) {
                new google.maps.Geocoder().geocode({
                    "location" : new google.maps.LatLng(
                        parseFloat(address[0]),
                        parseFloat(address[1])
                    )
                }, function(geocoderResults) {
                    if ($.isFunction(callback)) {
                        callback(geocoderResults);
                    }
                });
            }
        },

        getMapTypes : function(mapTypeNamesArray) {
            var mapNamesToMapTypeMap = {
                "map" : google.maps.MapTypeId.ROADMAP,
                "satellite" : google.maps.MapTypeId.SATELLITE,
                "hybrid" : google.maps.MapTypeId.HYBRID,
                "terrain" : google.maps.MapTypeId.TERRAIN
            }

            if (!$.isArray(mapTypeNamesArray))
                return mapNamesToMapTypeMap[mapTypeNamesArray.toString().toLowerCase()];

            return $.map(mapTypeNamesArray, function(mapTypeName) {
                return mapNamesToMapTypeMap[mapTypeName.toString().toLowerCase()];
            });
        },

        getMarkerObject : function($markerInput) {
            if ($markerInput.length) {
                var markerObject = {
                    "title" : $markerInput.attr("title")
                };
                var markerValue = $.trim($markerInput.val());
                if (markerValue) {
                    if (markerValue.indexOf(",") === -1) {
                        markerObject.location = markerValue;
                    } else {
                        markerObject.location = markerValue.split(",", 2);
                    }

                    return markerObject;
                }
            }

            return null;
        },

        getFieldSetAsParams : function($mapContainer) {
            var params = {};

            $("fieldset.hidden input", $mapContainer).each(function() {
                params[this.name] = this.value;
            });

            return params;
        }
    };

    var $mapContainers = $(".gmap");
    if ($mapContainers.length) {
        window.initGoogleMapsMacros = function() {
            $mapContainers.each(function() {
                GoogleMaps.init($(this));
            });
        };

        var firstGmapInstance = $($mapContainers[0]),
            lang = $("input[name='lang']", firstGmapInstance).val(),
            licenseKey = $("input[name='apiKey']", firstGmapInstance).val();

        $.getScript("//maps.googleapis.com/maps/api/js?sensor=false&callback=initGoogleMapsMacros&language=" + encodeURIComponent(lang) + "&key=" + encodeURIComponent(licenseKey));
    }
});