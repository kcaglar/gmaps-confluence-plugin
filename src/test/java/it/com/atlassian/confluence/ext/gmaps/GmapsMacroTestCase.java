package it.com.atlassian.confluence.ext.gmaps;

public class GmapsMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private static final String DEFAULT_WIDTH = "700";
    private static final String DEFAULT_HEIGHT = "500";
    private static final String DEFAULT_ZOOM = "13";
    private static final String DEFAULT_MAPTYPE = "Map,Satellite,Hybrid";
    private static final String DEFAULT_DISPLAYMAP = "Map";
    private static final String DEFAULT_CONTROLS = "large";
    private static final String DEFAULT_MAPSCALE = "true";
    private static final String DEFAULT_MAPTYPECONTROL = "true";
    private static final String DEFAULT_OVERVIEW = "false";
    private static final String DEFAULT_DRAG = "true";
    private static final String DEFAULT_DOUBLECLICKZOOM = "true";
    private static final String DEFAULT_WHEELZOOM = "false";
    private static final String DEFAULT_MARKER = "true";
    
    private String body = "Malaysia";
    private String spaceKey = "ds";
    
    public void testErrorMessageWithoutBodyContent()
    {
        String errorMessage = "There are following validation errors: No locations specified and no directions requested";
        
        final long pageId = createPage(spaceKey, "testErrorMessageWithoutBodyContent", "{gmap}{gmap}");
        
        viewPageById(pageId);
        
        assertElementPresentByXPath("//div[@class='wiki-content']//div[@class='errorBox']");
    }
    
    public void testWithoutParameters()
    {
        final long pageId = createPage(spaceKey, "testWithoutParameters", "{gmap}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithoutParametersAndNoMarker()
    {
        String body = "37.269135,-121.873398";
        
        final long pageId = createPage(spaceKey, "testWithoutParametersAndNoMarker", "{gmap}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centerlocation']", "value"));
    }

    public void testWithZoomParameterAndMultipleCityMarker()
    {
        String cityOne = "Chicago";
        String cityTwo = "San Francisco";
        String cityThree = "Seattle";
        
        String body = cityOne + cityTwo + cityThree;
        String zoom = "3";
        
        final long pageId = createPage(spaceKey, "testWithZoomParameterAndMultipleCityMarker", "{gmap:zoom="+zoom+"}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, zoom, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithMapTypeParameter()
    {
        String mapType = "Map,Satellite,Hybrid,Terrain";
        
        final long pageId = createPage(spaceKey, "testWithMapTypeParameter", "{gmap:maptype="+mapType+"}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, mapType, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithDisplayMapParameter()
    {
        String displayMap = "Satellite";
        
        final long pageId = createPage(spaceKey, "testWithDisplayMapParameter", "{gmap:displaymap="+displayMap+"}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, displayMap,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithControlsParameter()
    {
        String controls = "medium";
        
        final long pageId = createPage(spaceKey, "testWithControlsParameter", "{gmap:controls="+controls+"}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                controls, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithMapScaleParameter()
    {
        String mapScale = "false";
        
        final long pageId = createPage(spaceKey, "testWithMapScaleParameter", "{gmap:mapscale="+mapScale+"}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, mapScale, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithMapTypeControlParameter()
    {
        String mapTypeControl = "false";
        
        final long pageId = createPage(spaceKey, "testWithMapTypeControlParameter", "{gmap:maptypecontrol="+mapTypeControl+"}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, mapTypeControl, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithOverviewParameter()
    {
        String overview = "true";
        
        final long pageId = createPage(spaceKey, "testWithOverviewParameter", "{gmap:overview="+overview+"}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, overview, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithDragParameter()
    {
        String drag = "false";
      
        final long pageId = createPage(spaceKey, "testWithDragParameter", "{gmap:drag="+drag+"}"+body+"{gmap}");
      
        viewPageById(pageId);
      
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, drag, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
      
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithDoubleClickZoomParameter()
    {
        String doubleClickZoom = "false";
      
        final long pageId = createPage(spaceKey, "testWithDoubleClickZoomParameter", "{gmap:doubleclickzoom="+doubleClickZoom+"}"+body+"{gmap}");
      
        viewPageById(pageId);
      
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                doubleClickZoom, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
      
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWheelZoomParameter()
    {
        String wheelZoom = "true";
      
        final long pageId = createPage(spaceKey, "testWheelZoomParameter", "{gmap:wheelzoom="+wheelZoom+"}"+body+"{gmap}");
      
        viewPageById(pageId);
      
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, wheelZoom, DEFAULT_MARKER);
      
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testWithMarkerParameter()
    {
        String marker = "false";
        
        final long pageId = createPage(spaceKey, "testWithMarkerParameter", "{gmap:marker="+marker+"}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, marker);
        
        assertEquals(body,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='centeraddress']", "value"));
    }
    
    public void testDirectionsInBody()
    {
        String from = "24th and Mission SF CA";
        String to = "Geary and 20th SF CA";
        String body = "directions:from=" + from + "|to=" + to;
        
        final long pageId = createPage(spaceKey, "testDirectionsInBody", "{gmap}"+body+"{gmap}");
        
        viewPageById(pageId);
        
        assertGoogleMapAttributes(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_ZOOM, DEFAULT_MAPTYPE, DEFAULT_DISPLAYMAP,
                DEFAULT_CONTROLS, DEFAULT_MAPSCALE, DEFAULT_MAPTYPECONTROL, DEFAULT_OVERVIEW, DEFAULT_DRAG, 
                DEFAULT_DOUBLECLICKZOOM, DEFAULT_WHEELZOOM, DEFAULT_MARKER);
        
        assertEquals(from,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@class='directions']", "value"));
        assertEquals(to,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@class='directions']", "title"));
    }
    
    private void assertGoogleMapAttributes(String width, String height, String zoom, String mapType, String displayMap,
            String controls, String mapScale, String mapTypeControl, String overview, String drag, String doubleClickZoom,
            String wheelZoom, String marker)
    {
        assertEquals(width,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='width']", "value"));
        assertEquals(height,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='height']", "value"));
        assertEquals(zoom,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='zoom']", "value"));
        assertEquals(mapType,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='maptype']", "value"));
        assertEquals(displayMap,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='displaymap']", "value"));
        assertEquals(controls,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='controls']", "value"));
        assertEquals(mapScale,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='mapscale']", "value"));
        assertEquals(mapTypeControl,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='maptypecontrol']", "value"));
        assertEquals(overview,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='overview']", "value"));
        assertEquals(drag,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='drag']", "value"));
        assertEquals(doubleClickZoom,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='doubleclickzoom']", "value"));
        assertEquals(wheelZoom,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='wheelzoom']", "value"));
        assertEquals(marker,
                getElementAttributByXPath("//div[@class='wiki-content']//div//fieldset//input[@name='marker']", "value"));
    }
}
