package com.atlassian.confluence.ext.gmaps;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GmapsMacroTestCase extends TestCase
{
    private static final String DEFAULT_TEMPLATE = "/templates/gmaps/gmaps-macro.vm";
    
    private static final int DEFAULT_WIDTH = 700;
    private static final int DEFAULT_HEIGHT = 500;
    private static final int DEFAULT_ZOOM = 13;
    private static final String DEFAULT_MAPTYPE = "Map,Satellite,Hybrid";
    private static final String DEFAULT_DISPLAYMAP = "Map";
    // Default is auto but size calculated from width and height will change it to large
    private static final String DEFAULT_CONTROLS = "large"; 
    private static final boolean DEFAULT_MAPSCALE = true;
    private static final boolean DEFAULT_MAPTYPECONTROL = true;
    private static final boolean DEFAULT_OVERVIEW = false;
    private static final boolean DEFAULT_DRAG = true;
    private static final boolean DEFAULT_DOUBLECLICKZOOM = true;
    private static final boolean DEFAULT_WHEELZOOM = false;
    private static final boolean DEFAULT_MARKER = true;
    
    private static final String DUMMY_KEY = "abcdef-dummy-key-fedcba";
    private static final String DUMMY_HOST = "dummy.com";
    private static final String DUMMY_URL = "http://" + DUMMY_HOST + "/confluence";

    private String body = "Malaysia";
    
    private GmapsMacro gmapsMacro;

    @Mock private VelocityHelperService velocityHelperService;
    @Mock private LocaleManager localeManager;
    @Mock private I18NBeanFactory i18NBeanFactory;
    @Mock private ApiKeyManager apiKeyManager;
    @Mock private I18NBean i18n;
    @Mock private ConfluenceUser user;
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        AuthenticatedUserThreadLocal.set(user);

        when(localeManager.getLocale(Matchers.<User>anyObject())).thenReturn(Locale.getDefault());
        when(i18NBeanFactory.getI18NBean(Matchers.<Locale> anyObject())).thenReturn(i18n);
        when(i18n.getText(anyString(), Matchers.<List> anyObject())).thenAnswer(
                new Answer<String>()
                {
                    @Override
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return (String) invocationOnMock.getArguments()[0];
                    }
                }
        );

        gmapsMacro = new GmapsMacro(velocityHelperService, localeManager, i18NBeanFactory, apiKeyManager);
    }
    
    @Override
    protected void tearDown() throws Exception
    {
        AuthenticatedUserThreadLocal.setUser(null);
        super.tearDown();
    }

    public void testRenderWithDefaultParameters() throws MacroException
    {
        assertNull(gmapsMacro.execute(
                new HashMap<String, String>(),
                body,
                (ConversionContext) null
        ));
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return  isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && velocityContext.get("width").equals(DEFAULT_WIDTH)
                                        && velocityContext.get("height").equals(DEFAULT_HEIGHT)
                                        && velocityContext.get("zoom").equals(DEFAULT_ZOOM)
                                        && DEFAULT_MAPTYPE.equals(velocityContext.get("maptype"))
                                        && DEFAULT_DISPLAYMAP.equals(velocityContext.get("displaymap"))
                                        && DEFAULT_CONTROLS.equals(velocityContext.get("controls"))
                                        && velocityContext.get("mapscale").equals(DEFAULT_MAPSCALE)
                                        && velocityContext.get("maptypecontrol").equals(DEFAULT_MAPTYPECONTROL)
                                        && velocityContext.get("overview").equals(DEFAULT_OVERVIEW)
                                        && velocityContext.get("drag").equals(DEFAULT_DRAG)
                                        && velocityContext.get("doubleclickzoom").equals(DEFAULT_DOUBLECLICKZOOM)
                                        && velocityContext.get("wheelzoom").equals(DEFAULT_WHEELZOOM)
                                        && velocityContext.get("marker").equals(DEFAULT_MARKER);
                            }
                        }
                )
        );
    }
    
    public void testValidationFailsWhenMacroBodyEmpty() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>(),
                        "",
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return 24 == velocityContext.size()
                                        && user.equals(velocityContext.get("remoteUser"))
                                        && ((List) velocityContext.get("invalidParams")).isEmpty()
                                        && 1 == ((List) velocityContext.get("validationErrors")).size()
                                        && Arrays.asList("gmaps.errors.no.content").equals(velocityContext.get("validationErrors"))
                                        && ((List) velocityContext.get("locations")).isEmpty()
                                        && ((List) velocityContext.get("addresses")).isEmpty()
                                        && ((List) velocityContext.get("directions")).isEmpty();
                            }
                        }
                )
        );
    }

    public void testRenderWithCustomDimensions() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("width", "640");
                                put("height", "480");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && velocityContext.get("width").equals(640)
                                        && velocityContext.get("height").equals(480);
                            }
                        }
                )
        );
    }

    public void testRenderWithCustomZoom() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("zoom", "5");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && velocityContext.get("zoom").equals(5);
                            }
                        }
                )
        );
    }

    public void testRenderWithCustomMapTypes() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("maptype", "Map,Satellite");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && "Map,Satellite".equals(velocityContext.get("maptype"));
                            }
                        }
                )
        );
    }

    public void testRenderWithCustomDisplayMap() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("displaymap", "Map,satelite,terrain");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && "Map,satelite,terrain".equals(velocityContext.get("displaymap"));
                            }
                        }
                )
        );
    }

    public void testRenderWithMediumControls() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("controls", "medium");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && "medium".equals(velocityContext.get("controls"));
                            }
                        }
                )
        );
    }

    public void testRenderWithMapScaleDisabled() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("mapscale", "false");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && Boolean.FALSE.equals(velocityContext.get("mapscale"));
                            }
                        }
                )
        );
    }

    public void testRenderWithCustomMapTypeControlEnabled() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("maptypecontrol", "false");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && Boolean.FALSE.equals(velocityContext.get("maptypecontrol"));
                            }
                        }
                )
        );
    }

    public void testRenderWithOverviewEnabled() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("overview", "true");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && Boolean.TRUE.equals(velocityContext.get("overview"));
                            }
                        }
                )
        );
    }

    public void testRenderWithDraggingDisabled() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("drag", "false");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && Boolean.FALSE.equals(velocityContext.get("drag"));
                            }
                        }
                )
        );
    }

    public void testRenderWithoutDoubleClickZoom() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("doubleclickzoom", "false");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && Boolean.FALSE.equals(velocityContext.get("doubleclickzoom"));
                            }
                        }
                )
        );
    }

    public void testRenderWithoutWheelZoom() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("wheelzoom", "false");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && Boolean.FALSE.equals(velocityContext.get("wheelzoom"));
                            }
                        }
                )
        );
    }

    public void testRenderWithoutMarker() throws MacroException
    {
        assertNull(
                gmapsMacro.execute(
                        new HashMap<String, String>()
                        {
                            {
                                put("marker", "false");
                            }
                        },
                        body,
                        (ConversionContext) null
                )
        );
        verify(velocityHelperService).getRenderedTemplate(
                anyString(),
                argThat(
                        new ArgumentMatcher<Map<String, ?>>()
                        {
                            @Override
                            public boolean matches(Object _velocityContext)
                            {
                                @SuppressWarnings("unchecked")
                                Map<String, ?> velocityContext = (Map<String, ?>) _velocityContext;

                                return isVelocityContextContainingRequiredCommonParameters(DEFAULT_TEMPLATE, velocityContext)
                                        && Boolean.FALSE.equals(velocityContext.get("marker"));
                            }
                        }
                )
        );
    }
    
    private boolean isVelocityContextContainingRequiredCommonParameters(
            String templateName, Map<String, ?> velocityContext)
    {
        return StringUtils.equals(DEFAULT_TEMPLATE, templateName)
                && 24 == velocityContext.size()
                && user.equals(velocityContext.get("remoteUser"))
                && ((List) velocityContext.get("invalidParams")).isEmpty()
                && ((List) velocityContext.get("validationErrors")).isEmpty()
                && ((List) velocityContext.get("locations")).isEmpty()
                && ((List) velocityContext.get("addresses")).isEmpty()
                && ((List) velocityContext.get("directions")).isEmpty();
    }
}
