package com.atlassian.confluence.ext.gmaps;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestGmapsMacro extends TestCase
{
    private static final String EMPTY_BODY_STRING = "";
    private static final String ATLASSIAN_ADDRESS = "16th and Alabama SF CA";
    private static final String SOMEPLACE = "1000 Main St. Some place, NM";
    private static final String MULTIPLE_PLACES = ATLASSIAN_ADDRESS + "\naddress=1.5,2.3\n2.3,4.5\naddress=" + SOMEPLACE + "\ndirections:from=" + ATLASSIAN_ADDRESS + "|to=" + SOMEPLACE;
    
    private GmapsMacro macroInstance;

    @Mock private VelocityHelperService velocityHelperService;

    @Mock private LocaleManager localeManager;

    @Mock private I18NBeanFactory i18NBeanFactory;

    @Mock private ApiKeyManager apiKeyManager;
    
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        this.macroInstance = new GmapsMacro(velocityHelperService, localeManager, i18NBeanFactory, apiKeyManager);
    }
    
    
    public void testDefaultMacroParams()
    {
        Map<String, String> emptyParams = new HashMap<String, String>();
        Map<String, Object> context = new HashMap<String, Object>();
        
        macroInstance.parseMacroParams(emptyParams, context);
        
        assertNull( "Map id: ", context.get("name") );
        assertEquals( "Width: ", new Integer(700), context.get("width") );
        assertEquals( "Height: ", new Integer(500), context.get("height") );
        assertTrue( "Style should be empty.", StringUtils.isEmpty((String) context.get("style")));
        assertEquals( "Zoom: ", new Integer(13), context.get("zoom") );
        assertEquals( "Maptype: ", "map,satellite,hybrid", context.get("maptype") );
        assertEquals( "Displaymap: ", "map", context.get("displaymap") );
        assertEquals( "Controls: ", "auto", context.get("controls") );
        assertEquals( "Mapscale: ", Boolean.TRUE, context.get("mapscale") );
        assertEquals( "Map type control: ", Boolean.TRUE, context.get("maptypecontrol") );
        assertEquals( "Overview no enabled: ", Boolean.FALSE, context.get("overview") );
        assertEquals( "Drag enabled: ", Boolean.TRUE, context.get("drag")  );
        assertEquals( "Wheelzoom not enabled: ", Boolean.FALSE, context.get("wheelzoom")  );
    }
    
    
    // test basic params
    public void testMacroParams()
    {
        Map<String, Object> expected = new HashMap<String, Object>();
        expected.put(GMapsParameter.NAME.toString(), "mymap");
        expected.put(GMapsParameter.WIDTH.toString(), new Integer(100));
        expected.put(GMapsParameter.HEIGHT.toString(), new Integer(500));
        expected.put(GMapsParameter.STYLE.toString(), "mystyle");
        expected.put(GMapsParameter.ZOOM.toString(), new Integer(13));
        expected.put(GMapsParameter.MAPSCALE.toString(), Boolean.TRUE);
        expected.put(GMapsParameter.MAPTYPECONTROL.toString(), Boolean.FALSE);
        expected.put(GMapsParameter.OVERVIEW.toString(), Boolean.TRUE);
        expected.put(GMapsParameter.WHEELZOOM.toString(), Boolean.FALSE);
         
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(GMapsParameter.NAME.toString(), "mymap");
        parameters.put(GMapsParameter.WIDTH.toString(), "100");
        parameters.put(GMapsParameter.STYLE.toString(), "mystyle");
        parameters.put(GMapsParameter.MAPTYPECONTROL.toString(), "false");
        parameters.put(GMapsParameter.OVERVIEW.toString(), "true");
        
        
        Map<String, Object> context = new HashMap<String, Object>();
        List<String> errors = macroInstance.parseMacroParams(parameters, context);
        
        assertTrue( "Shouldn't have any invalid params.", errors.isEmpty());
        
        for (String key : expected.keySet())
        {
            assertEquals(key, expected.get(key), context.get(key) );
        }
    }
    
    
    // test param constraints


    public void testEmptyBodyContents()
    {
        Map<String, Object> context = new HashMap<String, Object>();
        macroInstance.parseMacroBody(EMPTY_BODY_STRING, context);

        assertNotNull(context.get("locations"));
        assertEquals("Empty body should produce zero locations", Collections.EMPTY_LIST, context.get( "locations" ));
        assertNotNull(context.get("addresses"));
        assertEquals("Empty body should produce zero addresses", Collections.EMPTY_LIST, context.get( "addresses" ));
        assertNotNull(context.get("directions"));
        assertEquals("Empty body should produce zero directions", Collections.EMPTY_LIST, context.get( "directions" ));
    }

    public void testMultipleBodyContents()
    {
        Map<String, Object> context = new HashMap<String, Object>();
        macroInstance.parseMacroBody(MULTIPLE_PLACES, context);

        List<GeoCode> locations = (List<GeoCode>) context.get( "locations" );
        assertNotNull(locations);
        assertEquals("Should have two locations", 2, locations.size());
        assertEquals(locations.get(0), new GeoCode(1.5,2.3));
        assertEquals(locations.get(1), new GeoCode(2.3,4.5));

        Object center = context.get("centerAddress");
        assertNotNull(center);
        assertTrue(center instanceof Address);
        assertEquals(ATLASSIAN_ADDRESS, ((Address) center).getAddress());
        
        List<Address> addresses = (List<Address>) context.get( "addresses" );
        assertNotNull(addresses);
        assertEquals("Should have 1 address", 1, addresses.size());
        assertEquals(addresses.get(0).getAddress(), SOMEPLACE);

        List<Directions> directions = (List<Directions>) context.get( "directions" );
        assertNotNull(directions);
        assertEquals("Should have 1 directions", 1, directions.size());
        assertEquals(directions.get(0).getFromAddress(), ATLASSIAN_ADDRESS);
        assertEquals(directions.get(0).getToAddress(), SOMEPLACE);
    }
    
//    public void testInvalidBodyContents()
//    {
//        VelocityContext context = new VelocityContext();
//        
//        macroInstance.parseMacroBody(EMPTY_BODY_STRING, context);
//    }
    
    // test address parsing
    public void testAddressParsing()
    {
        Object result = macroInstance.parseAddress("");
        assertNull("Empty address should result in null", result);
        
        result = macroInstance.parseAddress(ATLASSIAN_ADDRESS);
        assertEquals(ATLASSIAN_ADDRESS, ((Address) result).getAddress());

        result = macroInstance.parseAddress("address=" + ATLASSIAN_ADDRESS);
        assertEquals(ATLASSIAN_ADDRESS, ((Address) result).getAddress());
    }
    
    // test latlon parsing
    public void testLatLonParsing()
    {
        Object result = macroInstance.parseAddress("1.5,2.3");
        assertTrue("Should get back a GeoCode object", result instanceof GeoCode );
        assertEquals( result, new GeoCode(1.5,2.3));

        result = macroInstance.parseAddress("address=1.5,-2.3");
        assertTrue("Should get back a GeoCode object", result instanceof GeoCode );
        assertEquals( result, new GeoCode(1.5,-2.3));

        result = macroInstance.parseAddress("address=-1.5,-2.3|info=information");
        assertTrue("Should get back a GeoCode object", result instanceof GeoCode );
        assertEquals( result, new GeoCode(-1.5,-2.3));

        result = macroInstance.parseAddress("info=information|address=1.5,2.3");
        assertTrue("Should get back a GeoCode object", result instanceof GeoCode );
        assertEquals( result, new GeoCode(1.5,2.3));

        result = macroInstance.parseAddress("info=information|-1.5,2.3");
        assertTrue("Should get back a GeoCode object", result instanceof GeoCode );
        assertEquals( result, new GeoCode(-1.5,2.3));
    }
    
    // test direction parsing
    public void testDirectionParsing()
    {
        Object result = macroInstance.parseDirection("from=" + ATLASSIAN_ADDRESS + "|to=" + SOMEPLACE);
        assertTrue("Should get back a Directions object", result instanceof Directions );
        
        Directions direction = (Directions) result;
        assertEquals( direction.getFromAddress(), ATLASSIAN_ADDRESS);
        assertEquals( direction.getToAddress(), SOMEPLACE);
        
        result = macroInstance.parseDirection("to=" + SOMEPLACE + "|from=" + ATLASSIAN_ADDRESS );
        assertTrue("Should get back a Directions object", result instanceof Directions );
        
        direction = (Directions) result;
        assertEquals( direction.getFromAddress(), ATLASSIAN_ADDRESS);
        assertEquals( direction.getToAddress(), SOMEPLACE);
        
    }
}
